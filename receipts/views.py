from django.shortcuts import render
from django.views.generic.list import ListView
from receipts.models import ExpenseCategory, Receipt, Account


# Create your views here.
class ExpenseCategoryListView(ListView):
    model = ExpenseCategory
    template_name = "receipts/expense_category_list.html"


class AccountListView(ListView):
    model = Account
    template_name = "receipts/account_list.html"


class ReceiptListView(ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)
